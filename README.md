# Project Title

Tumblr posts viewer

### Installing

Clone project and run:

```
npm install
```

### Running the project

Run:

```
npm run dev
```

Projct will be available at: http://localhost:8080/

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

import React, { Component } from 'react'
import jsonp from 'jsonp'
import User from 'components/Elements/User'
import Posts from 'components/Elements/Posts'
import More from 'components/Elements/More'

export default class PostsContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      type: '',
      start: 0,
      posts: [],
      isLoading: false,
    }
  }

  setType = type => {
    this.setState({
      type,
      start: 0,
    }, () => this.state.username && this.getPosts())
  }

  setUsername = username => {
    if(username === ''){
      this.resetPosts()
    }
    this.setState({
      username,
      start: 0,
    }, this.getPosts)
  }

  resetPosts = () => {
    this.setState({
      start: 0,
      username: '',
      posts: [],
      isLoading: false,
    })
  }

  getMore = () => {
    this.setState({
      start: this.state.start + 20,
    }, () => {
      this.getPosts(this.state.start)
    })
  }

  getPosts = (start = 0) => {
    const { username, type } = this.state
    this.setState({
      isLoading: true,
    })
    username !== ''
      && jsonp(`https://${username}.tumblr.com/api/read/json?start=${start}&type=${type}`, (err, data) => {
      this.setState({
        isLoading: false,
      })
      if(err) {
        console.log('fetch error')
        return this.setState({
          posts: [],
          isLoading: false,
        })
      }
      if(start === 0){
        return this.setState({
          posts: data.posts,
        })
      } else {
        return this.setState({
          posts: [
            ...this.state.posts,
            ...data.posts,
          ],
        })
      }
    })
  }

  render() {
    const { posts, type, isLoading } = this.state
    return (
      <div>
        <User setUsername={this.setUsername} setType={this.setType} isLoading={isLoading} />
        <Posts posts={posts} type={type} isLoading={isLoading} />
        {(posts.length !== 0 && !isLoading) && <More getMore={this.getMore}/>}
      </div>
    )
  }
}

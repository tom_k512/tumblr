import React from 'react'
import PostsContainer from 'containers/PostsContainer'
import classes from './style.css'

const Layout = () => (
  <div className={classes.Layout}>
    <PostsContainer />
  </div>
)

export default Layout

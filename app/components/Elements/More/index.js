import React, { PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import classes from './style.css'

const More = ({getMore}) => (
  <div className={classes.More}>
    <Button bsStyle="info" onClick={getMore}>More</Button>
  </div>
)

More.propTypes = {
  getMore: PropTypes.func.isRequired,
}

export default More

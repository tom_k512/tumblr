import React, { Component, PropTypes } from 'react'
import {
  ControlLabel,
  FormControl,
  Col,
  Row,
  } from 'react-bootstrap'
import Loading from 'react-loading'
import classes from './style.css'

const TYPES = [
  {
    label: 'All',
    value: '',
  },
  {
    label: 'Text',
    value: 'text',
  },
  {
    label: 'Quote',
    value: 'quote',
  },
  {
    label: 'Photo',
    value: 'photo',
  },
  {
    label: 'Link',
    value: 'link',
  },
  {
    label: 'Conversation',
    value: 'conversation',
  },
  {
    label: 'Video',
    value: 'video',
  },
  {
    label: 'Audio',
    value: 'audio',
  },
]

export default class User extends Component {
  handleChange = e => {
    const { setUsername } = this.props
    setUsername(e.target.value)
  }

  handleTypeChange = e => {
    const { setType } = this.props
    setType(e.target.value)
  }

  render() {
    const { isLoading } = this.props
    return (
      <div className={classes.User}>
            <div className={classes.User__item}>
              <ControlLabel>
                Username
              </ControlLabel>
              <FormControl
                type="text"
                placeholder="Enter text"
                onChange={this.handleChange}
              />
            </div>
            <div className={classes.User__item}>
              <ControlLabel>Posts type</ControlLabel>
              <FormControl
                componentClass="select"
                placeholder="select"
                onChange={this.handleTypeChange}>
                {
                  TYPES.map((option, i) => {
                    return <option key={i} value={option.value}>{option.label}</option>
                  })
                }
              </FormControl>
            </div>
            <div style={{width: '64px'}}>
              {isLoading && <Loading type='cylon' color='grey' />}
            </div>
      </div>
    )
  }
}

User.propTypes = {
  setUsername: PropTypes.func.isRequired,
  setType: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
}

import React, { PropTypes } from 'react'
import Post from 'components/Elements/Post'
import classes from './style.css'

const Posts = ({posts, isLoading}) => {
  const $posts = posts.map((post, i) => <Post key={i} data={post} />)
  return (
    <div className={classes.Posts}>
      {posts.length === 0 ? !isLoading
        && <div className={classes.Posts__zero}>No records</div> : !isLoading && $posts
      }
    </div>
  )
}

Posts.propTypes = {
  posts: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
}

export default Posts

import React, { PropTypes } from 'react'
import { Panel, Button } from 'react-bootstrap'
import ReactPlayer from 'react-player'
import classes from './style.css'

const pureString = text => {
  let result = null
  if(typeof text === 'undefined' || text === 'undefined'){
    result = ''
  } else if(text === null) {
    result = 'No text'
  } else {
    result = text.replace(/<\/?[^>]+(>|$)/g, "")

    if(result.length > 200){
      result = result.substring(0, 200) + '...'
    }
  }

  return result
}

const Post = ({data}) => (
    <div>
      {
        data.type === 'regular' && (
          <Panel
            className={classes.Panel__fixed}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            {pureString(data['regular-title'])}
          </Panel>
        )
      }

      {
        data.type === 'photo' && (
          <Panel
            className={classes.Panel__fixed}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            <img src={data['photo-url-250']} />
          </Panel>
        )
      }

      {
        data.type === 'quote' && (
          <Panel
            className={classes.Panel__fixed}
            style={{fontStyle: 'Italic'}}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            {pureString(data['quote-text'])}
          </Panel>
        )
      }

      {
        data.type === 'link' && (
          <Panel
            className={classes.Panel__fixed}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            <a target="_blank" href={data['link-url']}><div>{pureString(data['link-text'])}</div></a>
          </Panel>
        )
      }

      {
        data.type === 'conversation' && (
          <Panel
            className={classes.Panel__fixed}
            style={{textAlign: 'left'}}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            {
              data.conversation.map((item, i) => {
                if(i === 4){
                  return <div style={{textAlign: 'center'}}>...</div>
                }
                if(i>4){
                  return
                }
                return (
                  <div>
                    <strong>{item.label}</strong> : {item.phrase}
                  </div>
                )
              })
            }
          </Panel>
        )
      }

      {
        data.type === 'video' && (
          <Panel
            className={classes.Panel__fixed}
            style={{textAlign: 'center'}}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            { data['video-source'].includes('youtube')
              ? <ReactPlayer url={data['video-source']} width="265" height="200" />
              : <a target="_blank" href={data.url}><div>Watch the movie</div></a>}
          </Panel>
        )
      }

      {
        data.type === 'audio' && (
          <Panel
            className={classes.Panel}
            header={data.date}
            footer={<a target="_blank" href={data.url}><div>...</div></a>}>
            <div style={{fontSize: '12px'}}>
              {data['id3-album'] || ''}
              <br />
              {pureString(data['id3-artist']) + ' ' + pureString(data['id3-artist']) + ' ' + pureString(data['id3-title'])}
              <br />
              <br />
            </div>
            <div dangerouslySetInnerHTML={{__html: data['audio-player']}} />
          </Panel>
        )
      }
    </div>
)

Post.propTypes = {
  data: PropTypes.object.isRequired,
}

export default Post
